﻿namespace TableCreator
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Interfaces;

    public class CreateExpression : ICreatorExpression
    {
        private readonly CreateTableExpression newCreateExpression = new CreateTableExpression();

        public ICreateTable Create()
        {
            return newCreateExpression;
        }

        public string GetExpression()
        {
            var expression = newCreateExpression.Expression.Append(")");
            return expression.ToString();
        }
    }

    public class CreateTableExpression : ICreateTable, IColumnCreate, IColumnProperty, IColumnConstrain
    {
        public Table TableBody { get; set; }

        public IColumnCreate PrimaryKey(params string[] columnNames)
        {
            var name = new StringBuilder();
            if (columnNames.Length > 1)

                for (var i = 0; i <= columnNames.Length; i++)
                {
                    if (i == columnNames.Length - 1)
                    {
                        name.Append($"{columnNames[i]}");
                        break;
                    }

                    name.Append($"{columnNames[i]}, ");
                }
            else
                name.Append($"{columnNames.First()}");

            var property = $"PRIMARY KEY ({name}), ";
            TableBody.Column.Property = property;
            Expression.Append($"\n{property} \n");
            return this;
        }

        public IColumnCreate ForeignKey(string columnName, string refTableKey, string refTableColumnName)
        {
            var property = $"FOREIGN KEY({columnName}) " +
                           $"REFERENCE {refTableKey} " +
                           $"({refTableColumnName}), ";

            Expression.Append($"\n{property}\n");
            TableBody.Column.Property = property;
            return this;
        }

        public IColumnCreate IsNullable(bool isNullable)
        {
            if (isNullable)
            {
                Expression.Append("NULL,\n");
                return this;
            }

            Expression.Append("NOT NULL,\n");
            return this;
        }

        public IColumnProperty AddColumn(string name)
        {
            Expression.Append($"{name} ");
            TableBody.Column = new Column();
            TableBody.Columns.Add(TableBody.Column);
            return this;
        }

        public IColumnCreate AsBool()
        {
            Expression.Append("BOOL, \n");
            TableBody.Column.Type = "BOOL";
            return this;
        }

        public IColumnConstrain AsString()
        {
            Expression.Append("TEXT ");
            TableBody.Column.Type = "TEXT";
            return this;
        }

        public IColumnConstrain AsInt64()
        {
            Expression.Append("BIGINT ");
            TableBody.Column.Type = "BIGINT ";
            return this;
        }

        public IColumnConstrain AsDecimal(int size, int precision)
        {
            Expression.Append($"DECIMAL({size}, {precision}) ");
            TableBody.Column.Type = $"DECIMAL({size}, {precision}) ";
            return this;
        }

        public IColumnConstrain AsDouble32()
        {
            Expression.Append("REAL ");
            TableBody.Column.Type = "REAL ";
            return this;
        }

        public StringBuilder Expression { get; } = new StringBuilder();

        public IColumnCreate Table(string nameTable)
        {
            TableBody = new Table();
            Expression.Append($"CREATE TABLE {nameTable} \n(\n");
            TableBody.Column.Name = nameTable;
            return this;
        }
    }

    public class Table
    {
        public List<Column> Columns { get; set; } = new List<Column>();
        public Column Column { get; set; } = new Column();
    }

    public class Column
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Property { get; set; }
    }
}