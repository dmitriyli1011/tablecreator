﻿namespace TableCreator.Interfaces
{
    using System.Text;

    public interface ICreatorExpression
    {
        ICreateTable Create();
    }

    public interface ICreateTable
    {
        StringBuilder Expression { get; }
        IColumnCreate Table(string nameTable);
    }

    public interface IColumnCreate
    {
        IColumnProperty AddColumn(string name);
    }

    public interface IColumnProperty
    {
        IColumnCreate AsBool();
        IColumnConstrain AsString();
        IColumnConstrain AsInt64();
        IColumnConstrain AsDouble32();
        IColumnConstrain AsDecimal(int size, int precision = 0);
    }

    public interface IColumnConstrain
    {
        IColumnCreate PrimaryKey(params string[] columnNames);
        IColumnCreate ForeignKey(string columnName, string refTableKey, string refTableColumnName);
        IColumnCreate IsNullable(bool isNullable);
    }
}