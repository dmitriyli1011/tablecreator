﻿namespace TableCreator
{
    using System;

    internal class Program
    {
        private static void Main(string[] args)
        {
            var table = new CreateExpression();

            table.Create()
                .Table("TEST1")
                .AddColumn("col1").AsInt64().PrimaryKey("col1","col2")
                .AddColumn("col2").AsString().IsNullable(false)
                .AddColumn("col3").AsInt64().ForeignKey("col_fk", "TEST1", "col1")
                .AddColumn("col4").AsBool()
                .AddColumn("col5").AsDecimal(10).IsNullable(true)
                .AddColumn("col6").AsDouble32().IsNullable(true);

            Console.WriteLine(table.GetExpression());

        }
    }
}